// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Review Model
const reviewModel = require("../models/review.model");
const courseModel = require("../models/course.model");

const createReviewOfCourse = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;
    const {
        bodyStar,
        bodyNote
    } = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "Course ID is not valid!"
        })
    }

    // Star là số nguyên, và lớn hơn 0 nhỏ hơn hoặc bằng 5
    if(bodyStar === undefined || !(Number.isInteger(bodyStar) && bodyStar > 0 && bodyStar <= 5)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "Star is not valid!"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    var newReview = {
        _id:mongoose.Types.ObjectId(),
        stars: bodyStar,
        note: bodyNote
    }

    reviewModel.create(newReview, (err, data) => {
        if(err) {
            return response.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        // Khi tạo review xong cần thêm id review mới vào mảng reviews của course
        courseModel.findByIdAndUpdate(courseId, {
            $push: { reviews: data._id }
        }, (err1, data1) => {
            if(err1) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err1.message
                })
            }
            
            return response.status(201).json({
                status: "Create review successfully",
                data: data
            })
        })
    })
}

const getReviewById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const reviewId = request.params.reviewId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ReviewID không hợp lệ"
        })
    }

    // B3: Gọi Model lấy dữ liệu
    reviewModel.findById(reviewId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        if(data) {
            return response.status(200).json({
                status: "Get detail review successfully",
                data: data
            })
        } else {
            return response.status(404).json({
                status: "Not Found"
            })
        }
      
    })
}

const getAllReview = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    reviewModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all review successfully",
            data: data
        })
    })
}

const getAllReviewOfCourse = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "Course ID is not valid!"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    courseModel
        .findById(courseId)
        .populate("reviews")
        .exec((err, data) => {
            if(err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
            
            return response.status(200).json({
                status: "Get reviews successfully",
                data: data.reviews
            })
        })
}

const updateReviewByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const reviewId = request.params.reviewId;
    const body = request.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "ReviewID không hợp lệ"
        })
    }

    if (body.bodyStars !== undefined && !(Number.isInteger(body.bodyStars) && body.bodyStars > 0 && body.bodyStars <= 5)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Rate không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateReview = {}

    if (body.bodyStars !== undefined) {
        updateReview.stars = body.bodyStars
    }

    if (body.bodyNote !== undefined) {
        updateReview.note = body.bodyNote
    }

    reviewModel.findByIdAndUpdate(reviewId, updateReview, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        if(data) {
            return response.status(200).json({
                status: "Update reivew successfully",
                data: data
            })
        } else {
            return response.status(404).json({
                status: "Not Found"
            })
        }
       
    })
}

const deleteReviewByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const reviewId = request.params.reviewId;
    const courseId = request.params.courseId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "Review ID is not valid!"
        })
    }

    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            "status": "Bad Request",
            "Message": "Course ID is not valid!"
        })
    }

    // B3: Thao tác với CSDL
    reviewModel.findByIdAndDelete(reviewId, (err, data) => {
        if(err) {
            return response.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        courseModel.findByIdAndUpdate(courseId, {
            $pull: {reviews: reviewId}
        }, (err1, data1) => {
            if(err1) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err1.message
                })
            }

            return response.status(204).send()
        })
    })
}

module.exports = {
    getReviewById,
    createReviewOfCourse,
    getAllReview,
    getAllReviewOfCourse,
    updateReviewByID,
    deleteReviewByID
}